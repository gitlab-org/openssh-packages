#!/bin/bash

set -e

PACKAGES_DIR=`pwd`/packages
mkdir ${PACKAGES_DIR}

OPENSSH_VERSION=openssh-7.5p1
yum -y update; yum -y install rpm-build gcc make wget openssl-devel krb5-devel pam-devel libX11-devel xmkmf libXt-devel curl util-linux-ng openssh-server; yum clean all

cd /root
curl --remote-name https://mirrors.evowise.com/pub/OpenBSD/OpenSSH/portable/${OPENSSH_VERSION}.tar.gz
tar xzvf "${OPENSSH_VERSION}.tar.gz"

# Overwrite the supplied sshd.pam with the one included with CentOS
# because the bundled one doesn't work by default
cp /etc/pam.d/sshd ${OPENSSH_VERSION}/contrib/redhat/sshd.pam
mv ${OPENSSH_VERSION}.tar.gz{,.orig}
tar zcpf ${OPENSSH_VERSION}.tar.gz ${OPENSSH_VERSION}

mkdir -p /root/rpmbuild/{SOURCES,SPECS}
cp ./${OPENSSH_VERSION}/contrib/redhat/openssh.spec /root/rpmbuild/SPECS/
cp ${OPENSSH_VERSION}.tar.gz /root/rpmbuild/SOURCES/
cd /root/rpmbuild/SPECS

sed -i -e "s/%define no_gnome_askpass 0/%define no_gnome_askpass 1/g" openssh.spec
sed -i -e "s/%define no_x11_askpass 0/%define no_x11_askpass 1/g" openssh.spec
sed -i -e "s/BuildPreReq/BuildRequires/g" openssh.spec

rpmbuild -bb openssh.spec

cp /root/rpmbuild/RPMS/x86_64/*.rpm ${PACKAGES_DIR}
