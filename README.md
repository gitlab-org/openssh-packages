# Introduction

To perform [SSH key lookups via the GitLab database](https://docs.gitlab.com/ee/administration/operations/speed_up_ssh.html),
GitLab requires OpenSSH 6.9+ in order to support the `AuthorizedKeysCommand` feature.

Both CentOS 6 and 7 do not currently ship with a version of CentOS that supports that feature:

* CentOS 6 ships with [OpenSSH 5.3](http://vault.centos.org/6.9/os/Source/SPackages/openssh-5.3p1-122.el6.src.rpm)
* CentOS 7 ships with [OpenSSH 6.6](http://vault.centos.org/7.3.1611/os/Source/SPackages/openssh-6.6.1p1-31.el7.src.rpm)

To make it easier for CentOS users to upgrade OpenSSH versions, this
project builds OpenSSH 7.5 packages for CentOS 6 and CentOS 7 using
GitLab CI Docker images.

